package oli.num;
/**
 *
 * @author Oliver Ross
 */
public class RomanNumeralGenerator implements IRomanNumeralGenerator {
    private final NumeralObject [] numeralObjects = {
        new NumeralObject(1000,"M"),
        new NumeralObject(900,"CM"),
	new NumeralObject(500,"D"),
	new NumeralObject(400,"CD"),
	new NumeralObject(100,"C"),
	new NumeralObject(90,"XC"),
	new NumeralObject(50,"L"),
	new NumeralObject(40,"XL"),
	new NumeralObject(10,"X"),
	new NumeralObject(9,"IX"),
	new NumeralObject(5,"V"),
	new NumeralObject(4,"IV"),
	new NumeralObject(1,"I"),
        new NumeralObject(0,""),
    };
    
    public RomanNumeralGenerator(){} 
    
    @Override
    public String generate(int decimal) {
        return getNumeral(decimal);
    }
    private String getNumeral(int decimal) {
        NumeralObject obj = getNumeralObject(decimal);
        return getNumeral(decimal - obj.getDecimal(), obj.getNumeral());
    }
    private String getNumeral(int decimal, String numeral) {
        if(decimal==0){return numeral;}
        NumeralObject obj = getNumeralObject(decimal);
        return getNumeral(decimal - obj.getDecimal(), numeral + obj.getNumeral());
    }
    private NumeralObject getNumeralObject(int decimal){
        for (NumeralObject numeralObject : numeralObjects) {
            if (decimal >= numeralObject.getDecimal()) {
                return numeralObject;
            }
        }
        return numeralObjects[numeralObjects.length-1];
    }
}
