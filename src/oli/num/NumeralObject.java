package oli.num;
/**
 *
 * @author Oliver Ross
 */
public class NumeralObject {
    private final int _decimal;
    private final String _numeral;

    public NumeralObject(int decimal,String numeral) {
        _numeral = numeral;
        _decimal = decimal;
    }
    public String getNumeral(){
        return _numeral;
    }
    public int getDecimal(){
        return _decimal;
    }
}
