/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oli.num;

/**
 *
 * @author Oliver Ross
 */
public interface IRomanNumeralGenerator {
    public String generate(int decimal);
}