/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerals;

import oli.num.RomanNumeralGenerator;

/**
 *
 * @author Oliver Ross
 */
public class Numerals {
/**
 * @param args the command line arguments
 */
    public static void main(String[] args) {
        RomanNumeralGenerator generator = new RomanNumeralGenerator();
        for(String arg : args){
            try{
                System.out.println(Integer.parseInt(arg) + " in roman numerals is " + generator.generate(Integer.parseInt(arg)));
            }catch(NumberFormatException e){
                System.out.println("cannot convert " + arg + ", intgers only");
            }
        }
    }
}